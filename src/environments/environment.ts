// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyD_iNUzbWovG8gwjvUMqiAy3zBwuhUpC90",
    authDomain: "eelectronics-e8f91.firebaseapp.com",
    projectId: "eelectronics-e8f91",
    storageBucket: "eelectronics-e8f91.appspot.com",
    messagingSenderId: "1099249336670",
    appId: "1:1099249336670:web:22d892dca07e32d6730fcf",
    measurementId: "G-4929FWZR58",
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
