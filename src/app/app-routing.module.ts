import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { StoreComponent } from "./store/store.component";
import { HomeComponent } from "./home/home.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { ProductsComponent } from "./products/products.component";
import { ShoppingCartComponent } from "./shopping-cart/shopping-cart.component";
import { CheckOutComponent } from "./check-out/check-out.component";
import { OrderSuccessComponent } from "./order-success/order-success.component";
import { LoginComponent } from "./login/login.component";
import { AdminProductsComponent } from "./admin/admin-products/admin-products.component";
import { AdminOrdersComponent } from "./admin/admin-orders/admin-orders.component";

const routes: Routes = [
  // { path: "", redirectTo: "/home", pathMatch: "full" },
  // { path: "", component: HomeComponent },
  // { path: "store", component: StoreComponent },
  // { path: "products", component: ProductsComponent },
  // { path: "shopping-cart", component: ShoppingCartComponent },
  // { path: "check-out", component: CheckOutComponent },
  // { path: "order-success", component: OrderSuccessComponent },
  // { path: "login", component: LoginComponent },
  // { path: "admin/products", component: AdminProductsComponent },
  // { path: "admin/orders", component: AdminOrdersComponent },
  // { path: "**", component: PageNotFoundComponent },
];

@NgModule({
  // imports: [RouterModule.forRoot(routes)],
  // exports: [RouterModule],
})
export class AppRoutingModule {}
