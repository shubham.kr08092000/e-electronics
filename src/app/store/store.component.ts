import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, RouterLinkActive } from "@angular/router";
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument,
} from "angularfire2/firestore";
// import { AngularFireDatabase } from "@angular/fire/database";
import { Observable } from "rxjs";
import { MatCarousel, MatCarouselComponent } from "@ngmodule/material-carousel";

@Component({
  selector: "app-store",
  templateUrl: "./store.component.html",
  styleUrls: ["./store.component.css"],
})
export class StoreComponent implements OnInit {
  itemValue = "";
  offerValue = "";

  laptopValue = "";
  mobileValue = "";
  tvValue = "";
  speakerValue = "";

  items: Observable<any[]>;
  offers: Observable<any[]>;

  laptops: Observable<any[]>;
  mobiles: Observable<any[]>;
  speakers: Observable<any[]>;
  tvs: Observable<any[]>;

  public homeId;

  constructor(
    private route: ActivatedRoute,
    db: AngularFirestore
    // lp: AngularFirestore,
    // mb: AngularFirestore,
    // sp: AngularFirestore,
    // tv: AngularFirestore
  ) {
    this.items = db.collection("items").valueChanges();
    this.offers = db.collection("offers").valueChanges();

    this.laptops = db.collection("laptops").valueChanges();
    this.mobiles = db.collection("mobiles").valueChanges();
    this.tvs = db.collection("tvs").valueChanges();
    this.speakers = db.collection("speakers").valueChanges();
  }

  // onSubmit() {
  //   this.db.list('items').push( {content: this.itemValue} );
  //   this.itemValue = '';
  // }

  // cartValue() {
  //   var numProduct = document.getElementById("numProduct");
  //   numProduct.type = "number";
  // }

  ngOnInit() {
    let id = parseInt(this.route.snapshot.paramMap.get("id"));
    this.homeId = id;
  }

  ggs = [
    { try: 1, name: "Laptop", names: "laptops" },
    { try: 2, name: "Speaker", names: "speakers" },
    { try: 3, name: "Mobile", names: "mobiles" },
    { try: 4, name: "Tv", names: "tvs" },
  ];

  slides = [
    {
      image: "https://shubham0809200.github.io/fashion-store/image/offer2.jpg",
    },
    {
      image: "https://shubham0809200.github.io/fashion-store/image/offer2.jpg",
    },
    {
      image:
        "http://sridurgaenterprises.co.in/wp-content/uploads/2018/10/HP-Laptop-Offer.png",
    },
    {
      image: "https://shubham0809200.github.io/fashion-store/image/offer3.jpg",
    },
    {
      image:
        "http://sridurgaenterprises.co.in/wp-content/uploads/2018/10/HP-Laptop-Offer.png",
    },
  ];
}
