import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument,
} from "angularfire2/firestore";
// import { AngularFireDatabase } from "@angular/fire/database";
import { Observable } from "rxjs";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"],
})
export class HomeComponent implements OnInit {
  itemValue = "";
  featuredCategoryValue = "";
  featuredProductValue = "";
  latestProductValue = "";

  items: Observable<any[]>;
  featuredCategorys: Observable<any[]>;
  featuredProducts: Observable<any[]>;
  latestProducts: Observable<any[]>;

  constructor(private router: Router, aa: AngularFirestore) {
    this.items = aa.collection("items").valueChanges();
    this.featuredCategorys = aa.collection("featuredCategorys").valueChanges();
    this.featuredProducts = aa.collection("featuredProducts").valueChanges();
    this.latestProducts = aa.collection("latestProducts").valueChanges();
  }

  ngOnInit() {}

  // departments = [
  //   { id: 1, name: "Laptops", image: "laptop.jpg" },
  //   { id: 2, name: "Speaker", image: "speaker.jpg" },
  //   { id: 3, name: "Mobiles", image: "mobile.jpg" },
  //   { id: 4, name: "TV", image: "tv.jpg" },
  // ];

  onSelect(items) {
    this.router.navigate(["/home", items.id]);
  }
}
