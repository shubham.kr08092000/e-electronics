import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-not-found',
  template: `

   
    <h1>
    <br><br><br>
    &nbsp; &nbsp; &nbsp; &nbsp;  404! <br>
        page-not-found
    </h1>
  `,
  styles: [
    `
    h1{
      padding-top:205px;
      padding-left:675px;
    }

    `
  ]
})
export class PageNotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
