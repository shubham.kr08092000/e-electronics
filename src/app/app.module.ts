import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AngularFireModule } from "angularfire2";
import { AngularFireDatabaseModule } from "angularfire2/database";
import { AngularFirestoreModule } from "angularfire2/firestore";
import { AngularFireAuthModule } from "@angular/fire/auth";

import { environment } from "../environments/environment";
import { FormsModule } from "@angular/forms";
import { AppComponent } from "./app.component";

import { AppRoutingModule } from "./app-routing.module";
import { RouterModule } from "@angular/router";
import { DataTableModule } from "angular5-data-table";

import { MatButtonModule } from "@angular/material/button";

import { HomeComponent } from "./home/home.component";
import { StoreComponent } from "./store/store.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatCarouselModule } from "@ngmodule/material-carousel";
import { MaterialModule } from "./material/material.module";
import { NavbarComponent } from "./navbar/navbar.component";
import { ProductsComponent } from "./products/products.component";
import { ShoppingCartComponent } from "./shopping-cart/shopping-cart.component";
import { CheckOutComponent } from "./check-out/check-out.component";
import { OrderSuccessComponent } from "./order-success/order-success.component";
import { MyOrdersComponent } from "./my-orders/my-orders.component";
import { AdminProductsComponent } from "./admin/admin-products/admin-products.component";
import { AdminOrdersComponent } from "./admin/admin-orders/admin-orders.component";
import { LoginComponent } from "./login/login.component";
import { CustomFormsModule } from "ng2-validation";

import { AuthService } from "./auth.service";
import { AuthGuard } from "./auth-guard.service";
import { AdminAuthGuard } from "./admin-auth-guard.service";
import { UserService } from "./user.service";
import { ProductFormComponent } from "./admin/product-form/product-form.component";
import { CategoryService } from "./category.service";
import { ProductService } from "./product.service";
import { ProductFilterComponent } from "./products/product-filter/product-filter.component";
import { ProductCardComponent } from "./product-card/product-card.component";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    StoreComponent,
    PageNotFoundComponent,
    NavbarComponent,
    ProductsComponent,
    ShoppingCartComponent,
    CheckOutComponent,
    OrderSuccessComponent,
    MyOrdersComponent,
    AdminProductsComponent,
    AdminOrdersComponent,
    LoginComponent,
    ProductFormComponent,
    ProductFilterComponent,
    ProductCardComponent,
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    FormsModule,
    CustomFormsModule,
    BrowserAnimationsModule,
    MatCarouselModule.forRoot(),
    MaterialModule,
    AngularFireDatabaseModule,
    DataTableModule,

    MatButtonModule,

    RouterModule.forRoot(
      [
        { path: "", component: ProductsComponent },
        { path: "home", component: HomeComponent },
        { path: "store", component: StoreComponent },
        { path: "products", component: ProductsComponent },
        { path: "shopping-cart", component: ShoppingCartComponent },
        { path: "login", component: LoginComponent },
        {
          path: "check-out",
          component: CheckOutComponent,
          canActivate: [AuthGuard],
        },
        {
          path: "my/orders",
          component: MyOrdersComponent,
          canActivate: [AuthGuard],
        },
        {
          path: "order-success",
          component: OrderSuccessComponent,
          canActivate: [AuthGuard],
        },

        {
          path: "admin/products/new",
          component: ProductFormComponent,
          canActivate: [AuthGuard],
        },
        {
          path: "admin/products/:id",
          component: ProductFormComponent,
          canActivate: [AuthGuard, AdminAuthGuard],
        },
        {
          path: "admin/products",
          component: AdminProductsComponent,
          canActivate: [AuthGuard, AdminAuthGuard],
        },
        {
          path: "admin/orders",
          component: AdminOrdersComponent,
          canActivate: [AuthGuard, AdminAuthGuard],
        },
        // { path: "**", component: PageNotFoundComponent },
      ],
      { relativeLinkResolution: "legacy" }
    ),

    // FlexLayoutModule,
  ],
  exports: [AngularFireAuthModule, BrowserAnimationsModule],
  providers: [
    AuthService,
    AuthGuard,
    AdminAuthGuard,
    UserService,
    CategoryService,
    ProductService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
