import { OnInit } from "@angular/core";
import { Injectable } from "@angular/core";
import { AngularFirestore } from "angularfire2/firestore";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class CategoryService implements OnInit {
  products: Observable<any[]>;
  // menuItemValue = "";

  // menuItems = new Observable<any[]>();

  constructor(private db: AngularFirestore) {}

  ngOnInit(): void {
    throw new Error("Method not implemented.");
  }

  getAll() {
    // return this.db.collection<any[]>("/menuItems").valueChanges();

    return (this.products = this.db
      .collection<any>("/menuItems")
      .valueChanges({ idField: "key" }));
  }
}
