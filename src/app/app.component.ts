import { OnInit } from "@angular/core";
import { Component } from "@angular/core";
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument,
} from "angularfire2/firestore";
import { Observable } from "rxjs";

// import { HomeComponent } from "./home/home.component";
import { AuthService } from "./auth.service";
import { Router } from "@angular/router";
import { UserService } from "./user.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit {
  title = "embassy-electronics";

  itemValue = "";
  items: Observable<any[]>;

  constructor(
    db: AngularFirestore,
    private userService: UserService,
    private auth: AuthService,
    router: Router
  ) {
    auth.user$.subscribe((user) => {
      if (!user) return;
      userService.save(user);

      let returnUrl = localStorage.getItem("returnUrl");

      if (!returnUrl) return;

      localStorage.removeItem("returnUrl");
      router.navigateByUrl(returnUrl);
    });

    this.items = db.collection("items").valueChanges();
  }
  ngOnInit(): void {
    // throw new Error("Method not implemented.");
  }
}
