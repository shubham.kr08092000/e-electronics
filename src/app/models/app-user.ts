export interface AppUser {
  isAdmin: boolean;
  name: string;
  email: string;
}
