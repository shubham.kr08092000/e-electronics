import { Injectable } from "@angular/core";
import { AngularFirestore } from "angularfire2/firestore";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class ProductService {
  delete(id: any) {
    throw new Error("Method not implemented.");
  }

  constructor(private db: AngularFirestore) {}

  products: Observable<any[]>;

  create(product: string) {
    return this.db.collection("/products").add(product);
  }

  getAll() {
    return (this.products = this.db
      .collection<any>("/products")
      .valueChanges({ idField: "key" }));

    // return this.db
    // .collection<any>("./menuItems")
    // .snapshotChanges()
    // .pipe(
    //   map((actions) =>
    //     actions.map((a) => ({
    //       key: a.payload.doc.id,
    //       ...a.payload.doc.data(),
    //     }))
    //   )
    // );
  }

  get(productId: string) {
    return this.db.collection("/products" + productId);
  }

  update(productId: string, product) {
    return this.db
      .collection<any>("/products" + productId)
      .doc(product)
      .update(product);
  }
}
